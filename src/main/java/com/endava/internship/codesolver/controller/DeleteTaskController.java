package com.endava.internship.codesolver.controller;

import com.endava.internship.codesolver.logic.service.TaskServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/deleteTask")
@RequiredArgsConstructor
public class DeleteTaskController {

    private final TaskServiceImpl taskService;

    @GetMapping
    public String deleteTaskByID(HttpServletRequest request) {
        taskService.deleteTaskById(Long.parseLong(WebUtils.getCookie(request, "taskId").getValue()));
        return "redirect:/index";
    }
}
