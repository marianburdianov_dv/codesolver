package com.endava.internship.codesolver.logic.service;

import com.endava.internship.codesolver.model.entities.Category;
import com.endava.internship.codesolver.model.entities.Task;

import java.util.List;
import java.util.Map;

public interface CategoryService {

    Map<Long, String> getCategoryMap();

    Map<Category, List<Task>> getCategoryTaskMap();

    boolean addCategory(String name);

    void updateCategory(String oldName, String newName);
}
