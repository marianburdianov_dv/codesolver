package com.endava.internship.codesolver.logic.service;

import com.endava.internship.codesolver.model.dao.RoleDao;
import com.endava.internship.codesolver.model.dao.TaskDao;
import com.endava.internship.codesolver.model.dao.UserStatisticsDao;
import com.endava.internship.codesolver.model.dao.UserStatisticsDao;
import com.endava.internship.codesolver.model.dao.UserStatisticsDao;
import com.endava.internship.codesolver.model.entities.Category;
import com.endava.internship.codesolver.model.entities.Task;
import com.endava.internship.codesolver.model.entities.User;
import com.endava.internship.codesolver.model.entities.UserStatistics;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final TaskDao taskRepository;
    private final UserStatisticsDao userStatisticsRepository;
    private final UserService userService;
    private final StatisticService statisticService;
    private final CategoryService categoryService;
    private static final Random random = new Random();

    @Value("${tasksPerUser}")
    private int tasksPerUserProperty;

    @Override
    public Map<Long, String> getTasksForCurrentUser() {

        User user = userService.getCurrentUser();
        List<Task> tasks = getTasksForUser(user);

        return tasks.stream()
                .collect(Collectors.toMap(Task::getTaskId, Task::getTaskTitle));
    }

    @Override
    public Task findTaskById(Long id) {
        return taskRepository.findById(id)
                .orElseThrow(NoSuchElementException::new);
    }

    @Transactional
    @Override
    public void deleteTaskById(Long id) {
        userStatisticsRepository.deleteByTask(findTaskById(id));
        taskRepository.deleteTaskByTaskId(id);
    }
    @Override
    public List<Task> getTasksForUser(User user) {
        List<Task> taskList;

        if (userService.userIsAdministrator(user)) {
            taskList = taskRepository.findAll();

        } else if (userIsNew(user)) {
            taskList = getRandomTasks();
            statisticService.saveTasksForUser(taskList, user);

        } else {
            taskList = userStatisticsRepository.findByUser(user)
                    .stream()
                    .map(UserStatistics::getTask)
                    .collect(Collectors.toList());
        }

        return taskList;
    }

    private boolean userIsNew(User user) {
        return (userStatisticsRepository.countByUser(user) == 0);
    }


    private List<Task> getRandomTasks() {

        Map<Category, List<Task>> categoryMap = categoryService.getCategoryTaskMap();
        List<Task> selectedTasks = new LinkedList<>();

        for (List<Task> tasks : categoryMap.values())
            selectedTasks.add(tasks.remove(random.nextInt(tasks.size())));

        List<Task> remainingTasks = categoryMap.values()
                .stream()
                .flatMap(List::stream)
                .collect(Collectors.toCollection(LinkedList::new));

        int extraTasksCount = Math.min(tasksPerUserProperty - selectedTasks.size(), remainingTasks.size());

        while (extraTasksCount > 0) {
            selectedTasks.add(remainingTasks.remove(random.nextInt(remainingTasks.size())));
            extraTasksCount--;
        }

        return selectedTasks;
    }
}