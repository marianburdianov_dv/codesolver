package com.endava.internship.codesolver.logic.service;

import com.endava.internship.codesolver.logic.dto.UserRegistrationDto;
import com.endava.internship.codesolver.model.entities.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public interface UserService extends UserDetailsService {

    String ROLE_ADMIN = "ROLE_ADMIN";

    User findByLogin(String login);

    User save(UserRegistrationDto registration);

    User getCurrentUser();

    boolean userIsAdministrator(User user);

}
