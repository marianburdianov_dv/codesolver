package com.endava.internship.codesolver.model.dao;

import com.endava.internship.codesolver.model.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.lang.management.OperatingSystemMXBean;
import java.util.Optional;

@Repository
public interface UserDao extends CrudRepository<User, Long> {

    User findByLogin(String login);

}

