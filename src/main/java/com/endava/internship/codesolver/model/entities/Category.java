package com.endava.internship.codesolver.model.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "categories")
@EqualsAndHashCode (of = "categoryId")
@ToString (of = "name")
@Getter
@Setter
public class Category {
    @Id
    @GeneratedValue(generator = "increment")
    @Column(name="category_id")
    private long categoryId;

    @Column (name = "category_name")
    private String name;

    @OneToMany(mappedBy = "category")
    private Set<Task> tasks;
}
