package com.endava.internship.codesolver.model.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;

@Entity
@Getter
@Setter
@Table(name = "roles")
@NoArgsConstructor
public class Role {

    @Id
    @GeneratedValue(generator = "increment")
    @Column(name = "role_id")
    private Long id;

    @Column(name = "role_body")
    private String name;

    public Role(String name) {
        this.name = name;
    }

}
