package com.endava.internship.codesolver.model.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

@Entity
@Table(name = "tests")
@Getter
@Setter
@EqualsAndHashCode(of = "testId")
public class TestForTask {

    @Id
    @GeneratedValue(generator = "increment")
    @Column(name = "test_id", updatable = false, nullable = false)
    private Long testId;

    @ManyToOne
    @JoinColumn(name = "task_id")
    private Task task;

    @Column(name = "test_file")
    @Type(type = "text")
    private String testBody;

    @Override
    public String toString() {
        return testBody;
    }

}
