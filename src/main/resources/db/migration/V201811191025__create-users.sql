CREATE TABLE java_interns_project.users (
  user_id BIGINT NOT NULL,
  user_name VARCHAR(45) NOT NULL,
  password VARCHAR(45) NOT NULL,
  role VARCHAR(5) DEFAULT 'USER',
  PRIMARY KEY (user_id),
  UNIQUE INDEX user_name_UNIQUE (user_name ASC) VISIBLE,
  CHECK (role IN ('USER', 'ADMIN')));