CREATE TABLE java_interns_project.tasks (
  task_id BIGINT NOT NULL,
  task_title VARCHAR(100) NOT NULL,
  task_body VARCHAR(2000) NOT NULL,
  description VARCHAR(5000) NOT NULL DEFAULT 'this task has not been formulated yet',
  PRIMARY KEY (task_id));