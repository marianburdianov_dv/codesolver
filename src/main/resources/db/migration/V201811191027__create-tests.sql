CREATE TABLE java_interns_project.tests (
  test_id BIGINT NOT NULL,
  task_id BIGINT NOT NULL,
  test_file TEXT NOT NULL,
PRIMARY KEY (test_id),
INDEX test_task_id_idx (task_id ASC) VISIBLE,
CONSTRAINT test_task_id
FOREIGN KEY (task_id)
REFERENCES java_interns_project.tasks (task_id)
);