CREATE TABLE java_interns_project.users_tasks (
  user_id BIGINT NOT NULL,
  task_id BIGINT NOT NULL,
  attempts INT NOT NULL DEFAULT 0,
  passed TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (user_id, task_id),
  INDEX fk_task_id_idx (task_id ASC) VISIBLE,
  CONSTRAINT fk_task_id
  FOREIGN KEY (task_id)
  REFERENCES java_interns_project.tasks (task_id),
  CONSTRAINT fk_user_id
  FOREIGN KEY (user_id)
  REFERENCES java_interns_project.users (user_id)
);