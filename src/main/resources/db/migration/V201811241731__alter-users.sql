ALTER TABLE `java_interns_project`.`users`
MODIFY COLUMN role BIGINT;

ALTER TABLE `java_interns_project`.`users`
MODIFY COLUMN password varchar(200);
