CREATE TABLE java_interns_project.roles (
  role_id BIGINT NOT NULL,
  role_body VARCHAR(100) NOT NULL,
  PRIMARY KEY (role_id));