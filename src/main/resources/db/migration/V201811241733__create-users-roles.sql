CREATE TABLE java_interns_project.users_roles (
  user_id BIGINT NOT NULL,
  role_id BIGINT NOT NULL,
 
  PRIMARY KEY (user_id, role_id),
  INDEX fk_role_id_idx (role_id ASC) VISIBLE,
  CONSTRAINT fk_role_id
  FOREIGN KEY (role_id)
  REFERENCES java_interns_project.roles (role_id),
  CONSTRAINT fk_user_role_id
  FOREIGN KEY (user_id)
  REFERENCES java_interns_project.users (user_id)
);

