ALTER TABLE java_interns_project.tasks
ADD COLUMN granted_attempts INT NULL DEFAULT 3 AFTER description;