CREATE TABLE java_interns_project.categories
(
	category_id bigint(20) auto_increment,
	category_name varchar(100) not null,
	CONSTRAINT categories_pk
		PRIMARY KEY (category_id)
);

ALTER TABLE java_interns_project.tasks
	ADD category_id BIGINT(20) null;

ALTER TABLE java_interns_project.tasks
	ADD CONSTRAINT tasks_categories_category_id_fk
		FOREIGN KEY (category_id) REFERENCES categories (category_id)
			on update cascade;