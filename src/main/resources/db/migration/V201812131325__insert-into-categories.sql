INSERT INTO java_interns_project.categories (category_id, category_name)
VALUES (1, 'Operators'),
       (2, 'Arrays'),
       (3, 'Strings'),
       (4, 'Collections'),
       (5, 'Exceptions'),
       (6, 'Lambda & Streams'),
       (7, 'DateTime'),
       (8, 'Concurrency');

UPDATE java_interns_project.tasks
SET category_id = 6
WHERE category_id IS NULL;

ALTER TABLE java_interns_project.tasks modify category_id bigint not null;