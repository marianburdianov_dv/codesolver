package com.endava.internship.codesolver.controller;

import com.endava.internship.codesolver.controller.security.config.SecurityConfigurationForTests;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class DefaultControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private HomePageController controller;

    @BeforeEach
    public void login() {
        SecurityConfigurationForTests.securityConfig();
    }

    @AfterEach
    public void logout() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void contextLoads() {
        Assertions.assertThat(controller).isNotNull();
    }

    @Test
    public void indexPageTest() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().is(302))
                .andExpect(redirectedUrl("/index"));
    }

    @Test
    public void loginPageTest() throws Exception {
        mockMvc.perform(get("/login"))
                .andExpect(status().is(200));
    }
}