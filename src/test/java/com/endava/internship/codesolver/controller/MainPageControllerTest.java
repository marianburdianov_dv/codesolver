package com.endava.internship.codesolver.controller;

import com.endava.internship.codesolver.controller.security.config.SecurityConfigurationForTests;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class MainPageControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void login() {
        SecurityConfigurationForTests.securityConfig();
    }

    @AfterEach
    public void logout() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void getTaskByIdExpectError() throws Exception {
        mockMvc.perform(get("/main"))
                .andExpect(status().is(400));
    }
}