package com.endava.internship.codesolver.controller.security.config;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class SecurityConfigTest {

    @Autowired
    private MockMvc mvc;

    @BeforeEach
    public void login() {
        SecurityConfigurationForTests.securityConfig();
    }

    @AfterEach
    public void logout() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void requiresAuthentication() throws Exception {
        SecurityContextHolder.clearContext();
        mvc.perform(get("/"))
                .andExpect(status().is(302))
                .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    public void expectAuthenticationIsOk() throws Exception {
        mvc.perform(get("/index"))
                .andExpect(status().is(200));
    }

    @Test
    public void authenticationFailed() throws Exception {
        mvc.perform(formLogin().user("user").password("invalid"))
                .andExpect(redirectedUrl("/login?error"))
                .andExpect(unauthenticated());
    }
}