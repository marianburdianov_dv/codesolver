package com.endava.internship.codesolver.controller.security.config;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;
import java.util.Collections;

public final class SecurityConfigurationForTests {

    public static void securityConfig() {
        SecurityContextHolder.getContext().setAuthentication(new Authentication() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                return Collections.singletonList((GrantedAuthority) () -> "ROLE_USER");
            }

            @Override
            public Object getCredentials() {
                return "user";
            }

            @Override
            public Object getDetails() {
                return "user";
            }

            @Override
            public Object getPrincipal() {
                return "ROLE_USER";
            }

            @Override
            public boolean isAuthenticated() {
                return true;
            }

            @Override
            public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
            }

            @Override
            public String getName() {
                return "user";
            }
        });
    }
}
