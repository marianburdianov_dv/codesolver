package com.endava.internship.codesolver.logic.converter;


import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


 class RuntimeCompilerExecutorTest {

    private static final String PACKAGE = "package com.endava.internship.codesolver.logic.converter.generated;";

    @Test
     void shouldReturnTrueOnce() {
        final List<Boolean> expectedResult = new LinkedList<>();
        expectedResult.add(true);
        final String header = PACKAGE + "public class ";
        final String className = "MyClass";
        final String mockUserInputString = "";
        final String mockTestString = "private static boolean testMethod() {return true;}";
        final String code = header + className + " { " + mockUserInputString + mockTestString + "}";
        final RuntimeCompilerExecutor runtimeCompiler = new RuntimeCompilerExecutor();

        final List<Boolean> actualResult = runtimeCompiler.executeUserCode(className.trim(), code);

        assertEquals(expectedResult, actualResult);
    }

    @Test
     void shouldReturnEmptyList() {
        final List<Boolean> expectedResult = Collections.emptyList();
        final String header = PACKAGE + "public class ";
        final String className = "MyClass";
        final String mockUserInputString = "";
        final String mockTestString = "private static boolean testMethod() {return true}";
        final String code = header + className + " { " + mockUserInputString + mockTestString + "}";
        final RuntimeCompilerExecutor runtimeCompiler = new RuntimeCompilerExecutor();

        final List<Boolean> actualResult = runtimeCompiler.executeUserCode(className.trim(), code);

        assertEquals(expectedResult, actualResult);
    }

    @Test
     void shouldReturnTrueTwice() {
        final List<Boolean> expectedResult = new LinkedList<>();
        expectedResult.add(true);
        expectedResult.add(true);
        final String header = PACKAGE +
                "import com.endava.internship.codesolver.logic.converter.testentities.Privilege;\n" +
                "import com.endava.internship.codesolver.logic.converter.testentities.User;\n" +
                "import java.util.List;\n" +
                "import java.util.Optional;\n" +
                "import static java.util.Arrays.asList;\n" +
                "import static java.util.Collections.emptyList;\n" +
                "import static java.util.Collections.singletonList;\n" +
                "  public class ";
        final String className = "MyClass";
        final String mockUserInputString = "public double getAverageAgeForUsers(final List<User> users) {\n" +
                "return users.stream()\n" +
                ".mapToDouble(User::getAge)\n" +
                ".average()\n" +
                ".orElse(-1);\n" +
                "}\n";
        final String mockTestString = "private static final List<Privilege> ALL_PRIVILEGES = asList(Privilege.values());\n" +
                "    public boolean testShouldReturnAverageAgeForUsers() {\n" +
                "        final int expectedAverage = 23;\n" +
                "        final User user1 = new User(1L, \"John\", \"Doe\", 26, singletonList(Privilege.UPDATE));\n" +
                "        final User user2 = new User(2L, \"Greg\", \"Smith\", 30, singletonList(Privilege.UPDATE));\n" +
                "        final User user3 = new User(3L, \"Alex\", \"Smith\", 13, singletonList(Privilege.DELETE));\n" +
                "        final double averageAge = getAverageAgeForUsers(asList(user1, user2, user3));\n" +
                "        return (averageAge == expectedAverage);\n" +
                "    }\n" +
                "    public boolean testShouldReturnMinusOneInsteadOfAverageForEmptyList() {\n" +
                "        final int expectedAverage = -1;\n" +
                "        final double averageAge = getAverageAgeForUsers(emptyList());\n" +
                "        return (averageAge == expectedAverage);\n" +
                "    }\n";
        final String code = header + className + " { " + mockUserInputString + mockTestString + "}";
        final RuntimeCompilerExecutor runtimeCompiler = new RuntimeCompilerExecutor();

        final List<Boolean> actualResult = runtimeCompiler.executeUserCode(className.trim(), code);

        assertEquals(expectedResult, actualResult);
    }
}