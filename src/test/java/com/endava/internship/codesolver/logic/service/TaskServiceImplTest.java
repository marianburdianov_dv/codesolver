package com.endava.internship.codesolver.logic.service;

import com.endava.internship.codesolver.model.entities.Task;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class TaskServiceImplTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private TaskServiceImpl taskService;

    @BeforeEach
    public void setUp() {
        jdbcTemplate.update("INSERT INTO java_interns_project.tasks\n" +
                "(task_id,\n" +
                "task_title,\n" +
                "task_body,\n" +
                "description)\n" +
                "VALUES\n" +
                "(30000,\n" +
                "'task 1',\n" +
                "'task body',\n" +
                "'description');");
    }

    @AfterEach
    public void clean() {
        jdbcTemplate.update("DELETE FROM java_interns_project.tasks WHERE task_id = 30000;");
    }

    @Test
    public void shouldFindTaskById() {
        Task testTask = taskService.findTaskById(30000L);
        assertNotNull(testTask);
    }

    @Test
    void getMapOfTaskTitles() {
        //FIXME incompatible types
        /*
        int actualCount = ( taskService.getMapOfTaskTitles()).size();
        int expextedCount = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM java_interns_project.tasks", Integer.class);
        assertEquals(expextedCount, actualCount);
        */
    }
}