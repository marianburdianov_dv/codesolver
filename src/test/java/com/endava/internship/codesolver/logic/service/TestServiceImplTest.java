package com.endava.internship.codesolver.logic.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class TestServiceImplTest {

    @Autowired
    private TestServiceImpl testService;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @BeforeEach
    public void setUp() {
        jdbcTemplate.update("INSERT INTO java_interns_project.tasks\n" +
                "(task_id,\n" +
                "task_title,\n" +
                "task_body,\n" +
                "description)\n" +
                "VALUES\n" +
                "(40000,\n" +
                "'task 1',\n" +
                "'task body',\n" +
                "'description');");
        jdbcTemplate.update("INSERT INTO java_interns_project.tests\n" +
                "(test_id,\n" +
                "task_id,\n" +
                "test_file)\n" +
                "VALUES\n" +
                "(40000,\n" +
                "40000,\n" +
                "'boolean testTest(){return true;}');\n");
    }

    @AfterEach
    public void clean() {
        jdbcTemplate.update("DELETE FROM java_interns_project.tests WHERE test_id = 40000;");
        jdbcTemplate.update("DELETE FROM java_interns_project.tasks WHERE task_id = 40000;");
    }

    @Test
    void expectNoSuchTaskException() {
        assertThrows(IllegalArgumentException.class, () -> {
            testService.executeTestsForTask("", 3001L);
        }, "No task found!");
    }

    @Test
    void expectExecuteTestForTask() {
        //FIXME incompatible types below
        //String task = testService.executeTestsForTask("", 40000L);
        //assertNotNull(task);
    }
}