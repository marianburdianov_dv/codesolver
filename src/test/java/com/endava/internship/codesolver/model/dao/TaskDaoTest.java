package com.endava.internship.codesolver.model.dao;

import com.endava.internship.codesolver.model.entities.Task;
import com.endava.internship.codesolver.model.entities.TestForTask;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class TaskDaoTest {

    @Autowired
    TaskDao taskDao;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @BeforeEach
    public void setUp() {
        jdbcTemplate.update("INSERT INTO java_interns_project.tasks\n" +
                "(task_id,\n" +
                "task_title,\n" +
                "task_body,\n" +
                "description)\n" +
                "VALUES\n" +
                "(20000,\n" +
                "'task 1',\n" +
                "'task body',\n" +
                "'description');");
    }

    @AfterEach
    public void clean() {
        jdbcTemplate.update("DELETE FROM java_interns_project.tasks WHERE task_id = 20000;");
    }

    @Test
    public void shouldGetTaskById() {
        Optional<Task> testTask = taskDao.findById(20000L);
        assertTrue(testTask.isPresent());
    }

    @Test
    public void shouldGetListOfTasks() {
        int actualCount = ((List<Task>) taskDao.findAll()).size();
        int expextedCount = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM java_interns_project.tasks", Integer.class);
        assertEquals(expextedCount, actualCount);

    }

    @Test
    public void shouldGetListOfTestsByTaskId() {
        jdbcTemplate.update("INSERT INTO `java_interns_project`.`tests`\n" +
                "(`test_id`,\n" +
                "`task_id`,\n" +
                "`test_file`)\n" +
                "VALUES\n" +
                "(10000,\n" +
                "20000,\n" +
                "'String of test');\n");
        List<TestForTask> testList = taskDao.findById(20000L).orElse(new Task()).getTests();
        jdbcTemplate.update("DELETE FROM java_interns_project.tests  WHERE test_id = 10000; ");
        assertFalse(testList.isEmpty());

    }
}