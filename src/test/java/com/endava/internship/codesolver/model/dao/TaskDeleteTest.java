package com.endava.internship.codesolver.model.dao;

import com.endava.internship.codesolver.logic.service.TaskService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.jdbc.JdbcTestUtils;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class TaskDeleteTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private TaskService taskService;
    @Autowired
    private TaskDao taskDao;

    @BeforeEach
    public void setUp() {
        jdbcTemplate.update("INSERT INTO java_interns_project.users\n" +
                "    (user_id,\n" +
                "    user_name,\n" +
                "    password)\n" +
                "    VALUES\n" +
                "    (100000, 'ater', 'ater');");

        jdbcTemplate.update("INSERT INTO java_interns_project.tasks\n" +
                "(task_id,\n" +
                "task_title,\n" +
                "task_body,\n" +
                "description)\n" +
                "VALUES\n" +
                "(20000,\n" +
                "'task 1',\n" +
                "'task body',\n" +
                "'description');");

        jdbcTemplate.update("INSERT INTO java_interns_project.users_tasks\n" +
                "(user_id,\n" +
                "task_id,\n" +
                "attempts,\n" +
                "passed,\n" +
                "solution)\n" +
                "VALUES\n" +
                "(100000,\n" +
                "20000,\n" +
                "0,\n" +
                "0,\n" +
                "'');");
    }

    @AfterEach
    public void clean() {
        jdbcTemplate.update("DELETE FROM java_interns_project.users_tasks WHERE user_id = 100000;");
        jdbcTemplate.update("DELETE FROM java_interns_project.users WHERE user_id = 100000;");
    }

    @Test
    void assertThatTaskWasDeletedById() {
        int countOfTasks = JdbcTestUtils.countRowsInTableWhere(jdbcTemplate, "tasks", "task_id =20000");
        int countOfUsersTasks = JdbcTestUtils.countRowsInTableWhere(jdbcTemplate, "users_tasks", "task_id =20000");
        assertEquals(1, countOfTasks);
        assertEquals(1, countOfUsersTasks);

        taskService.deleteTaskById(20000L);

        countOfTasks = JdbcTestUtils.countRowsInTableWhere(jdbcTemplate, "tasks", "task_id =20000");
        countOfUsersTasks = JdbcTestUtils.countRowsInTableWhere(jdbcTemplate, "users_tasks", "task_id =20000");
        assertEquals(0, countOfTasks);
        assertEquals(0, countOfUsersTasks);
    }

    @Test
    void assertThatOnlyOneRowWasDeleted() {
        long initialCapacity = taskDao.count();

        taskService.deleteTaskById(20000L);
        long newCapacity = taskDao.count();

        assertEquals(initialCapacity - 1, newCapacity);
    }
}