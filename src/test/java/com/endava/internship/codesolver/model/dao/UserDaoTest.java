package com.endava.internship.codesolver.model.dao;


import com.endava.internship.codesolver.model.entities.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class UserDaoTest {

    @Autowired
    private UserDao userDao;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @BeforeEach
    public void setUp(){
        jdbcTemplate.update("INSERT INTO java_interns_project.users\n" +
                "    (user_id,\n" +
                "    user_name,\n" +
                "    password)\n" +
                "    VALUES\n" +
                "    (100000, 'ater', 'ater');" );
    }

    @AfterEach
    public void clean(){
        jdbcTemplate.update("DELETE FROM java_interns_project.users WHERE user_id = 100000;");
    }

    @Test
    public void getUserById() {
        final Optional<User> user = userDao.findById(100000L);
        assertEquals(Long.valueOf(100000L), user.orElse(new User()).getUserId());
    }
}
